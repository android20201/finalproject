package com.example.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.project.databinding.FragmentSelectBookBinding

class SelectBook : Fragment() {
    private lateinit var menu: com.example.project.model.Novel
    private lateinit var menu2: com.example.project.model.Cartoon
    private val myDataset = com.example.project.data.dataSource().novels()
    private val myDataset2 = com.example.project.data.dataSource().cartoons()
    private var itemId = 0
    private val navigationArgs: SelectBookArgs by navArgs()
    private var _binding: FragmentSelectBookBinding?=null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSelectBookBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // retrieve id
        itemId = navigationArgs.itemId

        //find id match with dataset
        myDataset2.forEach{
            if(it.cartoonID == itemId){
                menu2 = it
            }
        }
        myDataset.forEach {
            if (it.novelID == itemId) {
                menu = it
            }
        }
        if(navigationArgs.selectId==1){
            binding.apply {
                titleDetail.text = menu.novelname
                authorDetail.text = menu.novelauthor
                bookdetail.text = menu.noveldetail
                bookImage.setImageResource(menu.novelimageResourceId)

            }
        }else if(navigationArgs.selectId==2){
            binding.apply {
                titleDetail.text = menu2.cartoonname
                authorDetail.text = menu2.cartoonauthor
                bookdetail.text = menu2.cartoondetail
                bookImage.setImageResource(menu2.cartoonimageResourceId)

            }
        }

    }
}
