package com.example.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.project.databinding.FragmentCategoryBinding


var cate: String? =null;


class Category : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCategoryBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.novelbtn.setOnClickListener {
            cate = "novel";
            val action = CategoryDirections.actionCategoryToRecomment()
            findNavController().navigate(action)

        }
        binding.cartoonbtn.setOnClickListener {
            cate = "cartoon";
            val action = CategoryDirections.actionCategoryToRecomment()
            findNavController().navigate(action)
        }

    }

}

