package com.example.project.model

import androidx.annotation.DrawableRes

data class Cartoon (
    @DrawableRes
    val cartoonID: Int,
    val cartoonimageResourceId: Int,
    val cartoonname: String,
    val cartoonauthor: String,
    val cartoondetail: String
    )