package com.example.project.model

import androidx.annotation.DrawableRes


    data class Novel (
        val novelID: Int,
        @DrawableRes
        val novelimageResourceId: Int,
        val novelname: String,
        val novelauthor: String,
        val noveldetail: String
    )
