package com.example.project.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.data.dataSource
import com.example.project.model.Cartoon
import com.google.android.material.card.MaterialCardView

class listadaptercartoon (
    private val context: Context?,
    private val data:List<Cartoon>,
    private val onItemClick:(Cartoon) -> Unit
): RecyclerView.Adapter<listadaptercartoon.CartoonViewHolder>() {
    class CartoonViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val noImg: ImageView = view.findViewById<ImageView>(R.id.bookImg)
        val novelname: TextView = view.findViewById<TextView>(R.id.book_name)
        val novelauthor: TextView = view.findViewById<TextView>(R.id.book_author)
        val item: MaterialCardView = view.findViewById<MaterialCardView>(R.id.idMenu)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartoonViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.recomment_list_item, parent, false)
        return CartoonViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: CartoonViewHolder, position: Int) {
        val cartoon = data[position]
        holder.noImg.setImageResource(cartoon.cartoonimageResourceId)
        holder.novelname.text = cartoon.cartoonname
        holder.novelauthor.text = cartoon.cartoonauthor
        holder.item.setOnClickListener {
            onItemClick(cartoon)
        }
    }
}





