package com.example.project.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.data.dataSource
import com.example.project.model.Novel
import com.google.android.material.card.MaterialCardView

class listadapternovel (
    private val context: Context?,
    private val data:List<Novel>,
    private val onItemClick:(Novel) -> Unit
): RecyclerView.Adapter<listadapternovel.NovelViewHolder>() {
    class NovelViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val noImg: ImageView= view.findViewById<ImageView>(R.id.bookImg)
        val novelname: TextView= view.findViewById<TextView>(R.id.book_name)
        val novelauthor: TextView = view.findViewById<TextView>(R.id.book_author)
        val item: MaterialCardView = view.findViewById<MaterialCardView>(R.id.idMenu)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NovelViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.recomment_list_item, parent, false)
        return NovelViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: NovelViewHolder, position: Int) {
        val novel = data[position]
        holder.noImg.setImageResource(novel.novelimageResourceId)
        holder.novelname.text = novel.novelname
        holder.novelauthor.text = novel.novelauthor
        holder.item.setOnClickListener{
            onItemClick(novel)
        }

    }
}


