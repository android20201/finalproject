package com.example.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.project.adapter.listadaptercartoon
import com.example.project.adapter.listadapternovel
import com.example.project.databinding.FragmentRecommentBinding
import com.example.project.data.dataSource

class Recomment : Fragment() {
    private var _binding:FragmentRecommentBinding?=null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRecommentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = dataSource().novels()
        val data2 = dataSource().cartoons()
        val recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        if (cate == "novel") {
        recyclerView.adapter = listadapternovel(requireContext(),data) {

                val action = RecommentDirections.actionRecommentToSelectBook(itemId = it.novelID, selectId = 1)
                findNavController().navigate(action)
            }
        }else {
            recyclerView.adapter = listadaptercartoon(requireContext(), data2) {
                val action = RecommentDirections.actionRecommentToSelectBook(itemId = it.cartoonID, selectId = 2)
                findNavController().navigate(action)
            }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}